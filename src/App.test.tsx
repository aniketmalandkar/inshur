import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("render header", () => {
  const { getByText } = render(<App />);

  const companyName = getByText(/INSHUR/i);
  expect(companyName).toBeInTheDocument();
});
