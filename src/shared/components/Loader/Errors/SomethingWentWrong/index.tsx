import React from "react";

const SomethingWentWrong = () => {
  return (
    <React.Fragment>
      <h1 className="mt-5">Oops.!! Something went Wrong</h1>
      <h6 className="text-secondary">Please try again later.</h6>
    </React.Fragment>
  );
};

export default SomethingWentWrong;
