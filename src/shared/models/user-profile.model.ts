export interface IJobDetails {
  title: string;
  company: string;
}

export interface IFavorites {
  foods: string[];
  hobbies: string[];
}

export interface IAddress {
  city: string;
  country: string;
}

export interface IContact {
  phone: string;
  email: string;
}

export interface IUserProfile {
  title: string;
  firstName: string;
  lastName: string;
  description: string;
  address: IAddress;
  contact: IContact;
  job: IJobDetails;
  favorites: IFavorites;
  picture: string;
}
