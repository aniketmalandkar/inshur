import React from "react";
import Nav from "./components/Nav";
import "./App.css";
import Profile from "./components/Profile";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Nav></Nav>
      </header>

      <div className="body container">
        <Profile></Profile>
      </div>
    </div>
  );
}

export default App;
