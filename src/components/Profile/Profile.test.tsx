import { waitForElement } from "@testing-library/react";
import { mount, ReactWrapper, shallow } from "enzyme";
import React from "react";
import User from "../../mocks/Profile.json";
import Profile from "./";

describe("Profile component", () => {
  describe("Mounted Template check with invalid response", () => {
    const mockJsonPromise = Promise.reject("No data");
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
    });
    let wrapper: ReactWrapper;

    beforeEach(() => {
      jest
        .spyOn(global, "fetch")
        .mockImplementation(() => mockFetchPromise as any);
    });

    it("should validate content", async () => {
      wrapper = mount(<Profile />);
      await waitForElement(() => wrapper);
      await wrapper.update();

      const h1Tags = wrapper.find("h1");
      expect(h1Tags).toHaveLength(2);
      expect(h1Tags.first().text()).toEqual("About Us");
      expect(h1Tags.last().text()).toEqual("Oops.!! Something went Wrong");
    });
  });

  describe("Mounted Template check with valid response", () => {
    const mockJsonPromise = Promise.resolve(User);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
    });
    let wrapper: ReactWrapper;

    beforeEach(() => {
      jest
        .spyOn(global, "fetch")
        .mockImplementation(() => mockFetchPromise as any);
    });

    it("should validate content", async () => {
      wrapper = mount(<Profile />);
      await waitForElement(() => wrapper);
      await wrapper.update();

      const imgElement = wrapper.find("img").getElement();
      expect(imgElement.props.src).toEqual(User.picture);
      expect(imgElement.props.alt).toEqual(
        `${User.firstName} ${User.lastName} profile`
      );

      const h1Tags = wrapper.find("h1");
      expect(h1Tags).toHaveLength(2);
      expect(h1Tags.first().text()).toEqual("About Us");
      expect(h1Tags.last().text()).toEqual(
        `${User.title}. ${User.firstName} ${User.lastName}`
      );

      const h5Tags = wrapper.find("h5");
      expect(h5Tags).toHaveLength(1);
      expect(h5Tags.first().text()).toEqual(User.job.title);

      const h6Tags = wrapper.find("h6");
      expect(h6Tags).toHaveLength(2);
      expect(h6Tags.first().text()).toEqual("HELLO, I AM");
      expect(h6Tags.last().text()).toEqual(User.job.company);

      const tableElement = wrapper.find("tbody tr").getElements();
      expect(tableElement).toHaveLength(5);
      expect(tableElement[0].props.children[1].props.children).toEqual(
        User.contact.phone
      );
      expect(tableElement[1].props.children[1].props.children).toEqual(
        User.contact.email
      );
      expect(tableElement[2].props.children[1].props.children).toEqual([
        User.address?.city,
        ",",
        User.address?.country,
      ]);

      tableElement[3].props.children[1].props.children.props.children.forEach(
        (child: any, index: number) => {
          expect(child.props.children).toEqual(User.favorites.foods[index]);
        }
      );

      tableElement[4].props.children[1].props.children.props.children.forEach(
        (child: any, index: number) => {
          expect(child.props.children).toEqual(User.favorites.hobbies[index]);
        }
      );
    });
  });
});
