import React, { useState, useEffect } from "react";
import { IUserProfile } from "../../shared/models/user-profile.model";
import Loader from "../../shared/components/Loader";
import UserInfoTable from "../UserInfoTable";
import SomethingWentWrong from "../../shared/components/Loader/Errors/SomethingWentWrong";

const Profile = () => {
  const initialUserState: IUserProfile = {} as IUserProfile;
  const [user, setUser] = useState(initialUserState);
  const [loader, setLoader] = useState(false);
  const [isError, setError] = useState(false);

  useEffect(() => {
    setLoader(true);

    fetch("http://www.mocky.io/v2/5ed8da0431000055b2c4e8e4")
      .then((res) => res.json())
      .then((data: IUserProfile) => {
        setLoader(false);
        setUser(data);
      })
      .catch((err) => {
        setLoader(false);
        setError(true);
      });
  }, []);

  const loadUserData = () => {
    return (
      <div className="row">
        <div className="col-md-5 col-lg-4 mt-5">
          <img
            src={user.picture}
            alt={`${user.firstName} ${user.lastName} profile`}
            className="img-thumbnail"
          />
        </div>
        <div className="col-md-7 col-lg-8 mt-5">
          <h6 className="text-secondary">HELLO, I AM</h6>
          <h1>{`${user.title}. ${user.firstName} ${user.lastName}`}</h1>

          <h5>{user.job.title}</h5>

          <h6 className="text-secondary">{user.job.company}</h6>

          <p className="text-secondary mt-4">{user.description}</p>

          <UserInfoTable user={user} />
        </div>
      </div>
    );
  };

  return (
    <React.Fragment>
      <h1 className="mt-5 text-center">About Us</h1>
      {loader ? (
        <Loader />
      ) : isError ? (
        <SomethingWentWrong />
      ) : Object.keys(user).length > 0 ? (
        loadUserData()
      ) : (
        ""
      )}
    </React.Fragment>
  );
};

export default Profile;
