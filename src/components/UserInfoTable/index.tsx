import React, { FunctionComponent } from "react";
import { IUserProfile } from "../../shared/models/user-profile.model";

const UserInfoTable: FunctionComponent<{ user: IUserProfile }> = ({ user }) => {
  return (
    <table className="table borderless">
      <thead>
        <tr>
          <th scope="col" className="sr-only">
            User info type
          </th>
          <th scope="col" className="sr-only">
            User info
          </th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>
            <strong>Phone:</strong>
          </td>
          <td>{user.contact?.phone}</td>
        </tr>

        <tr>
          <td>
            <strong>Email:</strong>
          </td>
          <td>{user.contact?.email}</td>
        </tr>

        <tr>
          <td>
            <strong>Address:</strong>
          </td>
          <td>
            {user.address?.city},{user.address?.country}
          </td>
        </tr>

        <tr>
          <td>
            <strong>Favorite Foods:</strong>
          </td>
          <td>
            <ul>
              {user.favorites?.foods.map((food, index) => (
                <li key={index}>{food}</li>
              ))}
            </ul>
          </td>
        </tr>

        <tr>
          <td>
            <strong>Hobbies:</strong>
          </td>
          <td>
            <ul>
              {user.favorites?.hobbies.map((hobby, index) => (
                <li key={index}>{hobby}</li>
              ))}
            </ul>
          </td>
        </tr>
      </tbody>
    </table>
  );
};

export default UserInfoTable;
